var express = require("express");
var cors=require("cors")
var chargebee=require("./routes")


var app=express()
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors())
app.use('/',chargebee)
app.listen(3001,()=>{
    console.log("Server running on port 3001")
})
