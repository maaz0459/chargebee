import React, { Component } from 'react';
import { Switch, HashRouter as Router, Route } from "react-router-dom";
import Example from './components/Example'
import Plans from './Screen/Plans'
import 'antd/dist/antd.css'
class App extends Component {
  constructor(props) {
    super(props)
    window.Chargebee.init({
      site: "your-site-1615270197438-test",
      publishableKey: "test_fOx9jJi5lbJvRxSS5pUw04H6DBZPslbp"
    })
  }

  render() {
    return (
      <Router basename="/react/">
        <Switch>
        
          <Route exact path="/" component={Plans} />
          <Route exact path="/Example" component={Example} />

        </Switch>
      </Router>
    )
  }
}

export default App;
