import React, { Component } from 'react';
import {CardComponent} from "@chargebee/chargebee-js-react-wrapper";
import './Example.css'
import axios from 'axios'
export default class Example2 extends Component {

  constructor(props) {
    super(props);
    // Create a ref for card component
    this.cardRef = React.createRef()
    this.themes = THEMES;
    this.state = {
      token: "",
      error: "",
      loading: false,
      firstName: "",
      email: "",
      data:[],
      phone: "",
      // To change styles dynamically
      fontSize: 16,

      // Custom classes
      classes: {
        focus: 'focus',
        invalid: 'invalid',
        empty: 'empty',
        complete: 'complete',
      },
      
      // Custom styles
      styles: this.getIframeStyles(this.themes[0]),

      // Card icon toggle
      cardIcon: true,
    }
    this.handleChange = this.handleChange.bind(this);
    this.tokenize = this.tokenize.bind(this);
  }

  tokenize=async()=> {
    this.setState({loading: true});
    const {firstName,email,phone} =this.state
    axios({
      method: 'post',
      url: 'http://localhost:3001/chargebee',
      data: {
          name: firstName,
          email:email,
          phone:phone,
          planName:this.props.location.state.planName
      }
  }).then(res => {
    console.log(res)
    this.setState({loading: false});
    this.setState({data:res.data})

    
  }).catch(err => {
    this.setState({loading: false});
    
    console.log(err)
  });
    this.cardRef.current.tokenize({}).then((data) => {
     
    }).catch((error) => {
       });
  }

  handleChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }
  
  getIframeStyles = (theme) => {
    return {
      base: {
        color: theme['main-text'],
        fontWeight: '500',
        fontFamily: 'Lato,-apple-system, BlinkMacSystemFont, Segoe UI, Helvetica Neue, sans-serif',
        fontSize: '16px',
        fontSmoothing: 'antialiased',
        iconColor: theme['main-text'],

        ':focus': {
          // color: '#424770',
        },

        '::placeholder': {
          color: theme['placeholder'],
        },

        ':focus::placeholder': {
          color: theme['placeholder-focused'],
        },
      },
      invalid: {
        color: theme['invalid'] || '#FF7C4A',

        ':focus': {
          color: theme['invalid-focused'] || '#e44d5f',
        },
        '::placeholder': {
          color: theme['invalid-placeholder'] || '#FFCCA5',
        },
      },
    }
  }

  

 

  

  toggleCardIcon = () => {
    this.setState({
      cardIcon: !this.state.cardIcon
    })
  }

  // Clear the contents of the fields
  clear = () => {
    this.cardRef.current.clear();
  }

  focus = () => {
    this.cardRef.current.focus();
  }

  render() {
    const { cardIcon, styles, classes } = this.state;
    return (
      <div className="ex2 container">
        <div className="ex2-wrap">
          <div className="ex2-fieldset">
            <label className="ex2-field">                  
              <span className="ex2-label">Name</span>
              <input name="firstName" className={ this.state.firstName ? "ex2-input val" : "ex2-input"} type="text" placeholder="John Doe" value={this.state.firstName} onChange={this.handleChange} />
            </label>
            <label className="ex2-field">                  
              <span className="ex2-label">Email</span>
              <input name="email" className={ this.state.email ? "ex2-input val" : "ex2-input"} type="text" placeholder="john@comp.any" value={this.state.email} onChange={this.handleChange} />
            </label>
            <label className="ex2-field">                  
              <span className="ex2-label">Phone</span>
              <input name="phone" className={ this.state.phone ? "ex2-input val" : "ex2-input"} type="text" placeholder="+63 53242 32637" value={this.state.phone} onChange={this.handleChange} />
            </label>

            <label className="ex2-field">
              {/* Render card component in combined-mode */}
              <CardComponent ref={this.cardRef} className="ex2-input fieldset field"
                icon={cardIcon}
                styles={styles}
                classes={classes} 
                
              />
            </label>
          </div>
          <button type="submit" className={ this.state.loading ? "submit ex2-button" : "ex2-button"} onClick={this.tokenize}>Checkout</button>
          {this.state.error && <div className="error" role="alert">{this.state.error}</div>}
          {this.state.token && <div className="token" >{this.state.token}</div>}
          
        </div>
        {this.state.data&&<div style={{color:"white",wordBreak:"break-all"}}>{JSON.stringify(this.state.data)}</div>}
      </div>
    );
  }
}

const THEMES = [
  {
    'main-bg': '#262E3A',
    'secondary-bg': '#3e4b5b',
    'main-text': '#fff',
    'placeholder': '#939aa3',
    'placeholder-focused': '#ccc',
    'primary-btn': '#6EEDB6',
    'btn-text': '#1C2026',
    'primary-btn-focus': 'rgb(70, 203, 145)',
    'invalid': '#FF7C4A',
    'invalid-focused': '#e44d5f',
    'invalid-placeholder': '#FFCCA5',
  },
  {
    'main-bg': '#D5DBE7',
    'secondary-bg': '#fff',
    'main-text': '#000',
    'placeholder': '#abacbe',
    'placeholder-focused': '#abacbe',
    'primary-btn': '#4773D2',
    'btn-text': '#fff',
    'primary-btn-focus': '#3361c3',
    'invalid': '#E94745',
    'invalid-focused': '#e44d5f',
    'invalid-placeholder': '#FFCCA5'
  },
  {
    'main-bg': '#fca571',
    'secondary-bg': '#fff',
    'main-text': '#252857',
    'placeholder': '#9293AB',
    'placeholder-focused': '#666',
    'primary-btn': '#252857',
    'btn-text': '#fff',
    'primary-btn-focus': '#191c4a',
    'invalid': '#E94745',
    'invalid-focused': '#e44d5f',
    'invalid-placeholder': '#FFCCA5'
  },
  {
    'main-bg': '#544FB0',
    'secondary-bg': '#fff',
    'main-text': '#35A9BD',
    'placeholder': 'ddd',
    'placeholder-focused': '#ddd',
    'primary-btn': '#35A9BD',
    'btn-text': '#fff',
    'primary-btn-focus': 'rgb(29, 138, 157)',
  }
]