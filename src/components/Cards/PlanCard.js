import React, { useState } from 'react';
import { Card } from 'antd';
import {Link} from "react-router-dom"
const PlanCard = ({name,price,color,background}) => {
    return ( 
        <div>
            {/* <a href={`https://mhrenterprises-test.chargebee.com/hosted_pages/plans/${name}`}>
           <Card hoverable style={{maxWidth:"100%",boxShadow: "0px 1px 3px 0px rgba(0,0,0,0.66)",backgroundColor:background,borderRadius:"10px"}}>
    <p style={{textAlign:"center",fontWeight:"bold",color:color}}>{name}</p>
    <p style={{textAlign:"center",fontWeight:"bolder",fontSize:"35px",color:color
}}>{price}$</p>
  </Card></a> */}
  <Link to={{pathname:"/Example",state:{planName:name}}}>
           <Card hoverable style={{maxWidth:"100%",boxShadow: "0px 1px 3px 0px rgba(0,0,0,0.66)",backgroundColor:background,borderRadius:"10px"}}>
    <p style={{textAlign:"center",fontWeight:"bold",color:color}}>{name}</p>
    <p style={{textAlign:"center",fontWeight:"bolder",fontSize:"35px",color:color
}}>{price}$</p>
  </Card>
  
  </Link>
        </div>
     );
}
 
export default PlanCard;