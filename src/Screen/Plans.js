import React,{useEffect} from 'react'
import {Row,Col} from 'antd'
import PlanCard from './../components/Cards/PlanCard'
import {Link} from 'react-router-dom'
const plans=[
    {
        name:"Premium",
        price:"25",
        color:"white",
        background:"#0A2C66"
    },
    {
        name:"Basic",
        price:"10",
        color:"black",
        background:"white"
    }
]
const Plans = () => {
   
    useEffect(() => {
        
    }, [])
    
    return ( <>
        <div style={{display:"flex",justifyContent:"center"}}>
<div style={{width:"50%",marginTop:"15%"}}>
    <Row justify="space-between">
        {plans.map((d,i)=>{
 return  <Col key={i} lg={8} md={10} sm={10} xs={10}><PlanCard name={d.name} price={d.price} color={d.color} background={d.background}   /></Col>
        
        })}
    </Row>  
</div>


        </div>
   
        </>
     );
}
 
export default Plans;